const obj = {
  name: 'Lucas',
  lastName: 'Bittencourt',
};

console.log(obj); // { name: 'Lucas', lastName: 'Bittencourt' }
console.log({ ...obj, age: 19 }); // { name: 'Lucas', lastName: 'Bittencourt', age: 19 }

function consoleLog({ name, lastName }) {
  console.log(name, lastName);
}

consoleLog(obj); // Lucas Bittencourt

const array = ['Lucas', 'Bittencourt'];

console.log(array); // [ 'Lucas', 'Bittencourt' ]
console.log(...array); // Lucas Bittencourt
